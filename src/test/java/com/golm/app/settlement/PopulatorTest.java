package com.golm.app.settlement;

import static com.golm.app.settlement.entity.State.ALIVE_BLUE;
import static com.golm.app.settlement.entity.State.ALIVE_GREEN;
import static com.golm.app.settlement.entity.State.DEAD;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.golm.app.model.Generation;
import com.golm.app.settlement.entity.History;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Overall generator testing.
 */
public class PopulatorTest {

  @Test
  public void testGeneration() {
    // GIVEN
    Populator populator = new Populator(50, 50);
    History history = populator.getHistory();
    history.clear();

    Integer[][] prevGen = new Integer[][]
           {{DEAD,          DEAD,       ALIVE_BLUE,   ALIVE_BLUE,    DEAD},
            {DEAD,          DEAD,       ALIVE_BLUE,   ALIVE_BLUE,    DEAD},
            {DEAD,          DEAD,       DEAD,         DEAD,          DEAD},
            {ALIVE_GREEN,   DEAD,       DEAD,         DEAD,          DEAD},
            {ALIVE_GREEN,   DEAD,       DEAD,         ALIVE_GREEN,   DEAD}};

    Integer[][] expectedGen = new Integer[][]
           {{DEAD,         ALIVE_BLUE,  DEAD,         DEAD,          DEAD},
            {DEAD,         DEAD,        ALIVE_BLUE,   ALIVE_BLUE,    DEAD},
            {DEAD,         DEAD,        DEAD,         DEAD,          DEAD},
            {DEAD,         DEAD,        DEAD,         DEAD,          ALIVE_GREEN},
            {DEAD,         ALIVE_GREEN, ALIVE_BLUE,   ALIVE_GREEN,   DEAD}};

    history.add(prevGen);

    // WHEN
    Generation actual = populator.getGeneration(1);

    // THEN
    assertArrayEquals(expectedGen, actual.getSnapshot());
    assertArrayEquals(expectedGen, history.getLog().get(actual.getGenerationId()).getSnapshot());
  }

  @Test
  public void testAnotherPattern() {
    // GIVEN
    Populator populator = new Populator(50, 50);
    History history = populator.getHistory();
    history.clear();

    Integer[][] prevGen = new Integer[][]
           {{ALIVE_GREEN,    DEAD,          ALIVE_GREEN,    DEAD,          ALIVE_GREEN},
            {DEAD,           ALIVE_BLUE,    DEAD,           ALIVE_BLUE,    DEAD},
            {ALIVE_GREEN,    DEAD,          ALIVE_GREEN,    DEAD,          ALIVE_GREEN},
            {DEAD,           ALIVE_BLUE,    DEAD,           ALIVE_BLUE,    DEAD},
            {ALIVE_GREEN,    DEAD,          ALIVE_GREEN,    DEAD,          ALIVE_GREEN}};

    Integer[][] expectedGen = new Integer[][]
           {{DEAD,           DEAD,    ALIVE_GREEN,    DEAD,    DEAD},
            {DEAD,           DEAD,    DEAD,           DEAD,    DEAD},
            {ALIVE_GREEN,    DEAD,    DEAD,           DEAD,    ALIVE_GREEN},
            {DEAD,           DEAD,    DEAD,           DEAD,    DEAD},
            {DEAD,           DEAD,    ALIVE_GREEN,    DEAD,    DEAD}};


    history.add(prevGen);

    // WHEN
    Generation actual = populator.getGeneration(1);

    // THEN
    assertArrayEquals(expectedGen, actual.getSnapshot());
    assertArrayEquals(expectedGen, history.getLog().get(actual.getGenerationId()).getSnapshot());

  }

  @Test
  public void testRefresh() {
    // WHEN
    Populator populator = new Populator(50, 50);
    Generation refresh = populator.refresh(5, 5);

    // THEN
    assertNotNull(refresh);
    assertEquals(1, populator.getHistory().getLog().size());

    Integer[][] snapshotGen = populator.getHistory().getLastCreatedGen().getSnapshot();
    assertEquals(5, snapshotGen.length);
    assertEquals(5, snapshotGen[0].length);
  }

  @Test
  public void testRandomizer() {
    // WHEN
    Populator p = new Populator(50, 50);
    Integer[][] Integers = p.randomGen(10, 10);

    // THEN
    List<Integer> nullElements = Arrays.stream(Integers)
        .flatMap(Arrays::stream)
        .filter(cell -> cell == null)
        .collect(Collectors.toList());

    assertEquals(0, nullElements.size());
  }
}
