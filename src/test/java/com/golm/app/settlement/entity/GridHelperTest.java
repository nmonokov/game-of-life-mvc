package com.golm.app.settlement.entity;

import static com.golm.app.settlement.entity.State.ALIVE_BLUE;
import static com.golm.app.settlement.entity.State.ALIVE_GREEN;
import static com.golm.app.settlement.entity.State.DEAD;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for Grid.
 */
public class GridHelperTest {

  private GridHelper gridHelper;

  @Before
  public void setup() {
    Integer[][] prevGen = new Integer[][]
           {{DEAD,       ALIVE_BLUE,  ALIVE_BLUE,  ALIVE_BLUE,  DEAD},
            {ALIVE_BLUE, DEAD,        DEAD,        DEAD,        ALIVE_GREEN},
            {ALIVE_BLUE, DEAD,        DEAD,        DEAD,        ALIVE_GREEN},
            {ALIVE_BLUE, DEAD,        DEAD,        DEAD,        ALIVE_GREEN},
            {DEAD,       ALIVE_GREEN, ALIVE_GREEN, ALIVE_GREEN, DEAD}};

    gridHelper = new GridHelper(prevGen.length, prevGen[0].length, new Position(), prevGen);
  }

  @Test
  public void testTop() {
    // WHEN
    gridHelper.setPosition(new Position(2, 1));
    Integer top = gridHelper.top();

    assertEquals(ALIVE_BLUE, top);
  }

  @Test
  public void testTopInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(2, 0));
    Integer top = gridHelper.top();

    assertEquals(ALIVE_GREEN, top);
  }

  @Test
  public void testBottom() {
    // WHEN
    gridHelper.setPosition(new Position(2, 3));
    Integer bottom = gridHelper.bottom();

    // THEN
    assertEquals(ALIVE_GREEN, bottom);
  }

  @Test
  public void testBottomInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(2, 4));
    Integer bottom = gridHelper.bottom();

    // THEN
    assertEquals(ALIVE_BLUE, bottom);
  }

  @Test
  public void testRight() {
    // WHEN
    gridHelper.setPosition(new Position(3, 2));
    Integer right = gridHelper.right();

    // THEN
    assertEquals(ALIVE_GREEN, right);
  }

  @Test
  public void testRightInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(4, 2));
    Integer right = gridHelper.right();

    // THEN
    assertEquals(ALIVE_BLUE, right);
  }

  @Test
  public void testLeft() {
    // WHEN
    gridHelper.setPosition(new Position(1, 2));
    Integer left = gridHelper.left();

    // THEN
    assertEquals(ALIVE_BLUE, left);
  }

  @Test
  public void testLeftInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(0, 2));
    Integer left = gridHelper.left();

    // THEN
    assertEquals(ALIVE_GREEN, left);
  }

  @Test
  public void testTopRight() {
    // WHEN
    gridHelper.setPosition(new Position(2, 1));
    Integer topRight = gridHelper.topRight();

    // THEN
    assertEquals(ALIVE_BLUE, topRight);
  }

  @Test
  public void testTopRightInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(2, 0));
    Integer topRight = gridHelper.topRight();

    // THEN
    assertEquals(ALIVE_GREEN, topRight);
  }

  @Test
  public void testTopLeft() {
    // WHEN
    gridHelper.setPosition(new Position(2, 1));
    Integer topLeft = gridHelper.topLeft();

    // THEN
    assertEquals(ALIVE_BLUE, topLeft);
  }

  @Test
  public void testTopLeftInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(2, 0));
    Integer topLeft = gridHelper.topLeft();

    // THEN
    assertEquals(ALIVE_GREEN, topLeft);
  }

  @Test
  public void testBottomRight() {
    // WHEN
    gridHelper.setPosition(new Position(2, 3));
    Integer bottomRight = gridHelper.bottomRight();

    // THEN
    assertEquals(ALIVE_GREEN, bottomRight);
  }

  @Test
  public void testBottomRightInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(2, 4));
    Integer bottomRight = gridHelper.bottomRight();

    // THEN
    assertEquals(ALIVE_BLUE, bottomRight);
  }

  @Test
  public void testBottomLeft() {
    // WHEN
    gridHelper.setPosition(new Position(2, 3));
    Integer bottomLeft = gridHelper.bottomLeft();

    // THEN
    assertEquals(ALIVE_GREEN, bottomLeft);
  }

  @Test
  public void testBottomLeftInfinity() {
    // WHEN
    gridHelper.setPosition(new Position(2, 4));
    Integer bottomLeft = gridHelper.bottomLeft();

    // THEN
    assertEquals(ALIVE_BLUE, bottomLeft);
  }

}
