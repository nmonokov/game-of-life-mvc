package com.golm.app.settlement.entity;

import static com.golm.app.settlement.entity.State.ALIVE_BLUE;
import static com.golm.app.settlement.entity.State.ALIVE_GREEN;
import static com.golm.app.settlement.entity.State.DEAD;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Tests for Grid.
 */
public class GridTest {

  private Integer[][] prevGen;
  private Grid grid;

  @Before
  public void setup() {
    prevGen = new Integer[][]
           {{DEAD,       ALIVE_BLUE,  ALIVE_BLUE,  ALIVE_BLUE,  DEAD},
            {ALIVE_BLUE, DEAD,        DEAD,        DEAD,        ALIVE_GREEN},
            {ALIVE_BLUE, DEAD,        DEAD,        DEAD,        ALIVE_GREEN},
            {ALIVE_BLUE, DEAD,        DEAD,        DEAD,        ALIVE_GREEN},
            {DEAD,       ALIVE_GREEN, ALIVE_GREEN, ALIVE_GREEN, DEAD}};

    grid = new Grid(prevGen);
  }

  @Test
  public void testNext() {
    // WHEN
    Integer get = grid.get();
    grid.next();
    Integer next = grid.get();

    // THEN
    assertEquals(DEAD, get);
    assertEquals(ALIVE_BLUE, next);
  }

  @Test
  public void testNextRow() {
    // WHEN
    grid.setPosition(new Position(4, 0));
    grid.next();
    Integer next = grid.get();

    // THEN
    assertEquals(ALIVE_BLUE, next);
  }

  @Test
  public void testInspect() {
    // GIVEN
    List<Integer> surroundingCells =
        Arrays.asList(DEAD, ALIVE_GREEN, ALIVE_BLUE, DEAD,
            ALIVE_BLUE, ALIVE_GREEN, DEAD, DEAD);

    // WHEN
    List<Integer> inspect = grid.inspect();

    // THEN
    assertEquals(surroundingCells, inspect);
  }

  @Test
  public void testHasNextTrue() {
    // WHEN
    grid.setPosition(new Position());
    boolean hasNext = grid.hasNext();

    // THEN
    assertTrue(hasNext);
  }

  @Test
  public void testHasNextFalse() {
    // WHEN
    grid.setPosition(new Position(0,5));
    boolean hasNext = grid.hasNext();

    // THEN
    assertFalse(hasNext);
  }
}
