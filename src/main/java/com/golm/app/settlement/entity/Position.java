package com.golm.app.settlement.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  Current cursor position.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Position {

  private int x;
  private int y;

  /**
   * Increment position on one row.
   */
  void increment() {
    x++;
  }

  /**
   * Change position to another row and sets it from the start of it.
   */
  void nextRow() {
    x = 0;
    y++;
  }

}
