package com.golm.app.settlement.entity;

import java.util.Objects;

/**
 * State of the cell.
 */
public class State {

  public static final Integer DEAD = 0;
  public static final Integer ALIVE_GREEN = 1;
  public static final Integer ALIVE_BLUE = 2;

  public static boolean isAlive(Integer state) {
    return Objects.equals(state, ALIVE_BLUE) || Objects.equals(state, ALIVE_GREEN);
  }

}
