package com.golm.app.settlement.entity;

import lombok.AccessLevel;
import lombok.Setter;

/**
 * Helper for detecting surrounding cells
 */
class GridHelper {

  private int maxHeight;
  private int maxWidth;
  private Integer[][] prevGen;

  @Setter(AccessLevel.PACKAGE)
  private Position position;

  GridHelper(int maxHeight, int maxWidth, Position position, Integer[][] prevGen) {
    this.maxHeight = maxHeight;
    this.maxWidth = maxWidth;
    this.position = position;
    this.prevGen = prevGen;
  }

  Integer top() {
    int y = position.getY() - 1;
    int x = position.getX();

    if (y == -1) {
      y = maxHeight - 1;
    }
    if (x >= maxWidth) {
      x = 0;
    }
    return prevGen[y][x];
  }

  Integer bottom() {
    int y = position.getY() + 1;
    int x = position.getX();

    if (y == maxHeight) {
      y = 0;
    }
    if (x >= maxWidth) {
      x = 0;
    }

    return prevGen[y][x];
  }

  Integer right() {
    int x = position.getX() + 1;

    if (x >= maxWidth) {
      x = 0;
    }

    return prevGen[position.getY()][x];
  }

  Integer left() {
    int x = position.getX() - 1;

    if (x == -1) {
      x = maxWidth -1;
    }

    return prevGen[position.getY()][x];
  }

  Integer topRight() {
    int x = position.getX() + 1;
    int y = position.getY() - 1;

    if (x >= maxWidth) {
      x = 0;
    }
    if (y == -1) {
      y = maxHeight -1;
    }

    return prevGen[y][x];
  }

  Integer topLeft() {
    int x = position.getX() - 1;
    int y = position.getY() - 1;

    if (x == -1) {
      x = maxWidth - 1;
    }
    if (y == -1) {
      y = maxHeight -1;
    }

    return prevGen[y][x];
  }

  Integer bottomRight() {
    int x = position.getX() + 1;
    int y = position.getY() + 1;

    if (x >= maxWidth) {
      x = 0;
    }
    if (y == maxHeight) {
      y = 0;
    }

    return prevGen[y][x];
  }

  Integer bottomLeft() {
    int x = position.getX() - 1;
    int y = position.getY() + 1;

    if (x == -1) {
      x = maxWidth -1;
    }
    if (y == maxHeight) {
      y = 0;
    }

    return prevGen[y][x];
  }

}
