package com.golm.app.settlement.entity;

import com.golm.app.model.Generation;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Stores every generation with cursor on current.
 */
public class History {

  @Getter
  private Map<Integer, Generation> log = new HashMap<>(50);
  private Integer current = 0;

  /**
   * Add generation to history.
   *
   * @param gen generation to add as grid's snapshot
   * @return newly added generation
   */
  public Generation add(Integer[][] gen) {
    Generation newGeneration = new Generation(current, gen);
    log.put(current++, newGeneration);
    return newGeneration;
  }

  /**
   * Clears history.
   */
  public void clear() {
    log = new HashMap<>(50);
    current = 0;
  }

  /**
   * Gets first generation which is created when Populator instantiates.
   *
   * @return first generation
   */
  public Generation getFirst() {
    return log.get(0);
  }

  /**
   * Get last created generation. Cursor points on a new empty space after that gen.
   *
   * @return last created generation
   */
  public Generation getLastCreatedGen() {
    return log.get(current - 1);
  }

}
