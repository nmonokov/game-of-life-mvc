package com.golm.app.settlement.entity;

import lombok.AccessLevel;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

/**
 * Encapsulated Grid logic.
 */
public class Grid {

  private int maxHeight;
  private int maxWidth;
  private Integer[][] prevGen;
  private Integer[][] newGen;
  private GridHelper gridHelper;

  @Setter(AccessLevel.PACKAGE)
  private Position position;

  /**
   * Instantiate new generation array based on previous one.
   *
   * @param prevGen previous generation settlement
   */
  public Grid(Integer[][] prevGen) {
    this.prevGen = prevGen;
    maxHeight = prevGen.length;
    maxWidth = prevGen[0].length;
    newGen = new Integer[maxHeight][maxWidth];
    position = new Position();
    gridHelper = new GridHelper(maxHeight, maxWidth, position, prevGen);
  }

  /**
   * Get the cell's State on current position.
   *
   * @return cell's State
   */
  public Integer get() {
    return prevGen[position.getY()][position.getX()];
  }

  /**
   * Set state DEAD/ALIVE of a cell based on position.
   *
   * @param state state to be set
   */
  public void set(Integer state) {
    newGen[position.getY()][position.getX()] = state;
  }

  /**
   * Iterates through generation and moves cursor accordingly it's limits.
   */
  public void next() {
    int x = position.getX();

    if (x >= maxWidth - 1) {
      position.nextRow();
    } else {
      position.increment();
    }
  }

  /**
   * Inspects surrounding cells.
   *
   * @return list of all surrounding cells
   */
  public List<Integer> inspect() {
    return Arrays.asList(gridHelper.top(), gridHelper.topRight(),
                         gridHelper.right(),  gridHelper.bottomRight(),
                         gridHelper.bottom(),  gridHelper.bottomLeft(),
                         gridHelper.left(),  gridHelper.topLeft());
  }

  /**
   * Checks if there any element left. Assuming that after increment() from final element there
   * will be x = 0 and y = max width of an array.
   *
   * @return true/false
   */
  public boolean hasNext() {
    if (position.getX() == 0 && position.getY() == maxHeight) {
      return false;
    }
    return true;
  }

  /**
   * Gather new generation.
   *
   * @return new generation
   */
  public Integer[][] snapshot() {
    return newGen;
  }

}
