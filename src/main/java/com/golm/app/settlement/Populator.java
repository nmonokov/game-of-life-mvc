package com.golm.app.settlement;

import static com.golm.app.settlement.entity.State.ALIVE_BLUE;
import static com.golm.app.settlement.entity.State.ALIVE_GREEN;
import static com.golm.app.settlement.entity.State.DEAD;

import com.golm.app.model.Generation;
import com.golm.app.settlement.entity.Grid;
import com.golm.app.settlement.entity.History;
import com.golm.app.settlement.entity.State;
import com.golm.app.settlement.preset.Preset;

import lombok.Getter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Class for populating generations, storing history, randomizing generations.
 */
public class Populator {

  @Getter
  private History history;

  /**
   * Instantiate class with ready random generation
   */
  public Populator(Integer height, Integer width) {
    history = new History();
    history.add(randomGen(height, width));
  }

  /**
   * Generates new or returns previous (if exists) generation.
   *
   * @param selectedId selected id
   * @return new or previous generation
   */
  public Generation getGeneration(Integer selectedId) {
    Generation generation = history.getLog().get(selectedId);

    if (generation == null) {
      Integer[][] generationSnapshot = generate(history.getLastCreatedGen().getSnapshot());
      generation = history.add(generationSnapshot);
    }

    return generation;
  }

  private Integer[][] generate(Integer[][] prevGen) {
    Grid grid = new Grid(prevGen);

    while (grid.hasNext()) {
      Integer currCell = grid.get();
      fillCell(grid, currCell);
      grid.next();
    }

    return grid.snapshot();
  }


  private void fillCell(Grid grid, Integer currCell) {
    List<Integer> liveCells = grid.inspect().stream()
        .filter(State::isAlive)
        .collect(Collectors.toList());

    if (liveCells.size() < 2) {
      grid.set(State.DEAD);
    } else if ((liveCells.size() == 2 || liveCells.size() == 3) && State.isAlive(currCell)) {
      grid.set(currCell);
    } else if (liveCells.size() > 3 && State.isAlive(currCell)) {
      grid.set(DEAD);
    } else if (liveCells.size() == 3 && !State.isAlive(currCell)) {
      grid.set(chooseColor(liveCells));
    } else {
      grid.set(DEAD);
    }
  }

  private Integer chooseColor(List<Integer> liveCells) {
    List<Integer> blueCells = liveCells.stream()
        .filter(state -> Objects.equals(state, ALIVE_BLUE))
        .collect(Collectors.toList());

    if (blueCells.size() > liveCells.size() / 2) {
      return ALIVE_BLUE;
    } else {
      return ALIVE_GREEN;
    }
  }

  /**
   * Refreshes history, sets and return a random generation with 50% DEAD cells and 25%
   * live per each color.
   *
   * @param width desired width
   * @param height desired height
   * @return random generation
   */
  public Generation refresh(Integer height, Integer width) {
    history.clear();
    return history.add(randomGen(height, width));
  }

  Integer[][] randomGen(Integer height, Integer width) {
    Integer[][] randomGen = new Integer[height][width];

    Stack<Integer> states = randomStackOfStates(height * width);
    IntStream.range(0, height)
        .forEach(h ->
            IntStream.range(0, width)
                .forEach(w ->
                    randomGen[h][w] = states.pop()));

    return randomGen;
  }

  private Stack<Integer> randomStackOfStates(Integer quantity) {
    int deadCellsCount = quantity / 2;
    int liveCellsCount = (quantity - deadCellsCount) / 2;
    int leftCells = quantity - deadCellsCount - liveCellsCount;

    Stack<Integer> stack = new Stack<>();
    IntStream.range(0, deadCellsCount + leftCells).forEach(i -> stack.add(State.DEAD));
    IntStream.range(0, liveCellsCount).forEach(i -> stack.add(State.ALIVE_BLUE));
    IntStream.range(0, liveCellsCount).forEach(i -> stack.add(State.ALIVE_GREEN));
    Collections.shuffle(stack);

    return stack;

  }

  /**
   * Get's a preset by name and adding it to a history.
   *
   * @param name preset's name
   * @return preset's generation
   */
  public Generation getPreset(String name) {
    history.clear();
    return history.add(Preset.getByName(name));
  }

}
