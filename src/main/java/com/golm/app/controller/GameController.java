package com.golm.app.controller;

import com.golm.app.model.Generation;
import com.golm.app.settlement.Populator;
import com.golm.app.settlement.preset.Preset;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 * Game controller.
 */
@Controller
@SessionAttributes("populator")
public class GameController {

  @Value("${game-of-life.input-value.min}")
  private Integer minValue;

  @Value("${game-of-life.input-value.max}")
  private Integer maxValue;

  @Value("${game-of-life.default.height}")
  private Integer defaultHeight;

  @Value("${game-of-life.default.width}")
  private Integer defaultWidth;


  /**
   * Starts application with ready generation to display.
   *
   * @return index view and sets populator to a session
   */
  @GetMapping("/")
  public ModelAndView index() {
    Populator populator = new Populator(defaultHeight, defaultWidth);
    Generation generation = populator.getHistory().getFirst();

    ModelAndView mav = new ModelAndView("index");
    mav.addObject("populator", populator);
    mav.addObject("initialHeight", generation.getSnapshot().length);
    mav.addObject("initialWidth", generation.getSnapshot()[0].length);
    mav.addObject("presets", Preset.getNames());
    return mav;
  }

  /**
   * Pulls first generation from history
   *
   * @param populator session attribute populator
   * @return first generation
   */
  @GetMapping("/first")
  @ResponseBody
  public Generation firstGen(@ModelAttribute("populator") Populator populator) {
    return populator.getHistory().getFirst();
  }

  /**
   * Handles creation and retrieval of previous gens.
   *
   * @param populator session attribute populator
   * @param selectedId id of existing or not generation
   * @return generation to display
   */
  @GetMapping("/get/{id}")
  @ResponseBody
  public Generation getGeneration(@ModelAttribute("populator") Populator populator,
                                  @PathVariable("id") Integer selectedId) {

    Generation generation;
    if (selectedId > -1) {
      generation = populator.getGeneration(selectedId);
    } else {
      generation = populator.getHistory().getFirst();
    }

    return generation;
  }

  /**
   * Refreshes current gen's progress, clears history and create a new gen.
   *
   * @param populator session attribute populator
   * @param height desired height
   * @param width desired width
   * @return refreshed generation to display
   */
  @PostMapping("/refresh")
  @ResponseBody
  public Generation refresh(@ModelAttribute("populator") Populator populator,
                            @RequestParam("height") Integer height,
                            @RequestParam("width") Integer width) {

    return populator.refresh(validValue(height), validValue(width));
  }

  /**
   * Get's a preset by name.
   *
   * @param populator session attribute populator
   * @param presetName preset name
   * @return preset's generation to display
   */
  @GetMapping("/preset")
  @ResponseBody
  public Generation getPreset(@ModelAttribute("populator") Populator populator,
                              @RequestParam("presetName") String presetName) {

    return populator.getPreset(presetName);
  }

  private Integer validValue(Integer value) {
    if (value > maxValue) {
      return maxValue;
    }
    if (value < minValue) {
      return minValue;
    }
    return value;
  }
}
