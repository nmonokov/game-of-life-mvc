package com.golm.app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model for generation.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Generation {

  private Integer generationId;
  private Integer[][] snapshot;

}
