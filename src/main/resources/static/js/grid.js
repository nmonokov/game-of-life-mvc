var breaking = false;
var loopStarted = false;

/**
 * Initialize table with selected height and width.
 */
function init(height, width) {
    $('#height').val(height);
    $('#width').val(width);
    resizeCells(height, width);
    get('start');
}

/**
 * Fit cells into the 'grid-wrap' container by resizing them.
 */
function resizeCells(height, width) {
    var denominator;
    if (height > width) {
        denominator = height;
    } else {
        denominator = width;
    }

    var tdSize = 500 / denominator;
    $("<style type='text/css'> td{ height:" + tdSize + "px; width:" + tdSize + "px;} </style>")
        .appendTo("head");
    $('#grid-wrap').width(tdSize * width);
}

/**
 * Draws table and sets initial cell styles.
 */
function drawGrid(generation) {
    var genId = generation.generationId;
    var array = generation.snapshot;
    var grid = "";

    for (var row = 0; row < array.length; row++) {
        grid += "<tr>";
        for (var col = 0; col < array[0].length; col++) {
            grid += "<td id=\"_" + row + "_" + col + "\" class=\"_" + array[row][col] + "\"></td>";
        }
        grid += "</tr>";
    }

    document.getElementById('generationId').innerHTML = genId;
    document.getElementById('grid').innerHTML = grid;
}

/**
 * Fills grid with style from 'generation'. Vanilla JS approach for optimization reasons.
 */
function fillGrid(generation) {
    var genId = generation.generationId;
    var array = generation.snapshot;

    for (var row = 0; row < array.length; row++) {
        for (var col = 0; col < array[0].length; col++) {
            var id = '_' + row + '_' + col;
            var clazz = '_' + array[row][col];
            document.getElementById(id).className = clazz;
        }
    }
    document.getElementById('generationId').innerHTML = genId;
}

/**
 * Creates dropdown and fills it with presets from 'options'.
 */
function initDropdown(options) {
    // create the button
    var dropDownId = 'presetDropDown';
    var button = document.createElement('BUTTON');
    button.id = dropDownId;
    button.innerHTML = 'PATTERN';
    $('#insert-dropdown').append(button);

    // add the options to the button (unordered list)
    var ul = document.createElement('UL');
    ul.setAttribute('class', 'mdl-menu mdl-js-menu mdl-js-ripple-effect ');
    ul.setAttribute('for', dropDownId); // associate button

    for (var index in options) {
        // add each item to the list
        var li = document.createElement('LI');
        li.setAttribute('class', 'mdl-menu__item');
        li.innerHTML = options[index];
        li.button = button;
        li.onclick = onSelect;
        ul.appendChild(li);
    }
    $('#insert-dropdown').append(ul);
}

var onSelect = function() {
    var name = this.innerHTML;
    this.button.innerHTML = name;
    getPresetGrid(name.match(/(\w+)<span/)[1]);
}

/**
 * Handles calls to backend for Preset grid.
 */
function getPresetGrid(name) {
    breaking = true;
    loopStarted = false;

    $.ajax ({
        type: 'GET',
        url: '/preset',
        data: { 'presetName' : name },
        dataType: 'json'
    }).done(function(data) {
        height = data.snapshot.length;
        width = data.snapshot[0].length;
        resizeCells(height, width);
        drawGrid(data);
    });
}

/**
 * Refreshes the grid.
 */
function submitRefresh() {
    $('#refreshForm').submit(function(event) {
        breaking = true;
        loopStarted = false;

        $('#presetDropDown').html("PATTERN");
        var formData = {
            'width' : $('#width').val(),
            'height' : $('#height').val()
        };

        $.ajax ({
            type: 'POST',
            url: '/refresh',
            data: formData,
            dataType: 'json'
        }).done(function(data) {
            height = data.snapshot.length;
            width = data.snapshot[0].length;
            resizeCells(height, width);
            drawGrid(data);
        });

        event.preventDefault();
    });
}

/**
 * Handles next, prev and loop requests. Calls backend to get appropriate styles for grid cells.
 */
function get(mode, loop) {
    if (!breaking) {
        var selectedId = $('#generationId').text();
        var path = "";

        if (mode === "next") {
            path = "/get/" + (parseInt(selectedId) + 1)
        } else if (mode === "prev") {
            path = "/get/" + (parseInt(selectedId) - 1)
        } else if (mode === "start") {
            path = "/first";
        }

        $.ajax({
            url: path,
            async: true,
            success: function(result){
                if (mode === 'start') {
                    drawGrid(result);
                } else {
                    fillGrid(result);
                }
                if (loop) {
                    get('next', loop);
                }
        }});
    }
}

function prev() {
    breaking = false;
    get('prev');
}

function next() {
    breaking = false;
    get('next');
}

function loop() {
    breaking = false;
    if (!loopStarted) {
        loopStarted = true;
        get('next', true);
    }
}

function stop() {
    breaking = true;
    loopStarted = false;
}


